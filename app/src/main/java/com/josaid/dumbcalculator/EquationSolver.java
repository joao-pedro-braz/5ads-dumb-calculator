package com.josaid.dumbcalculator;

import android.util.Log;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.EvaluatorException;
import org.mozilla.javascript.ScriptableObject;

import java.util.UUID;

public final class EquationSolver {
    private static final String TAG = EquationSolver.class.getSimpleName();
    private final Context rhino = Context.enter();
    private final ScriptableObject scope = rhino.initStandardObjects();

    EquationSolver() {
        // Turn off optimizations
        rhino.setOptimizationLevel(-1);
    }

    public String solve(String equation) throws EvaluatorException {
        final UUID uuid = UUID.randomUUID();
        final String result = rhino.evaluateString(
                scope,
                equation,
                String.format("ES (%s)", uuid.toString()),
                1,
                null
        ).toString();

        Log.d(TAG, String.format("The equation [%s] resulted in: %s", equation, result));
        return result;
    }
}
